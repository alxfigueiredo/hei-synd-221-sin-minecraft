package ch.hevs.isi.Database;
import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.Core.IConnector;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

/** This is a singleton class
 *  It write FloatDataPoints or BooleanDataPoints into the Database
 */

public class DatabaseConnector implements IConnector  {

    /* *********************
     *  Declare variables  *
     * *********************/

    private String userName;
    private String password;
    private String hostName;
    private static DatabaseConnector db = null;                         // Singleton class's attribute

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Inform connector that the specified object has a new value
     *  @param bDataPoint the Boolean DataPoint
     */

    public void onNewValue(BooleanDataPoint bDataPoint)
    {
        String valueToPush;

        // Convert the value to a String
        if(bDataPoint.getValue() == true)
        {
            valueToPush = "1";
        }
        else
        {
            valueToPush = "0";
        }
        pushToDB(bDataPoint.getLabel(),valueToPush);                    // Push the new value into the Database
    }

    /** Inform connector that the specified object has a new value
     *  @param fDataPoint the float DataPoint
     */

    public void onNewValue(FloatDataPoint fDataPoint)
    {
        String valueToPush = String.valueOf(fDataPoint.getValue());     // Convert float value to a String
        pushToDB(fDataPoint.getLabel(),valueToPush);                    // Push the new value into the Database
    }

    private DatabaseConnector(String dBHostName, String dbUserName, String dbPassWord)
    {
        hostName = dBHostName;
        userName = dbUserName;
        password = dbPassWord;
    }

    private void pushToDB(String label, String value)
    {
        try {
            // Steps to perform an HTTP operation on a document identifier by a URL
            URL url = new URL("https://influx.sdi.hevs.ch/write?db=SIn03");                 // Construct an URL object
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();              // Instantiates a HttpURLConnection object and returns a reference to it

            // Configure authorisation based on HTTP basic authentication
            String userPass   = userName + ":" + password;
            String encoding = Base64.getEncoder().encodeToString(userPass.getBytes());
            connection.setRequestProperty ("Authorization", "Basic " + encoding);
            connection.setRequestMethod("POST");                                                  // Set HTTP method to POST
            connection.setRequestProperty("Content-Type", "binary/octet-stream");                 // Header Content_type set to binary/octet-stream
            connection.setDoOutput(true);                                                         // Indicate that there is a message body by invoking setDoOutput(true)

            // Fetch and OutputStreamWriter object for the HttpURLConnection
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            // Write the body of the HTTP Post request into the OutputStreamWriter
            String request = label + " value=" + Float.parseFloat(value);
            writer.write(request);
            writer.flush();

            // Fetch a BufferedReader object for the HttpURLConnection
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            // Wait for the response and check that its status code is "2'4" and read incoming data
            int responseCode = connection.getResponseCode();
            System.out.println("Status code : " + responseCode);
            while ((in.readLine()) != null) {System.out.println("error");}

            // Close all communication related objects
            in.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sOut = "DataBaseConnector <" + label + ", " + value + ">";
        System.out.println(sOut);
    }

    /** Get the reference to private attribute of the singleton class
     *  @return the singleton class's private instance
     */

    public static DatabaseConnector getInstance()
    {
        return getInstance("influx.sdi.hevs.ch", "SIn03", "0a9bab50a292656afd9881bf9310513f");
    }

    /** Get the reference to private attribute of the singleton class
     *
     * @param dbHostName the data base host name
     * @param dbUserName the data base user name
     * @param dbPassWord the data base password
     * @return the singleton class's private instance
     */

    public static DatabaseConnector getInstance(String dbHostName, String dbUserName, String dbPassWord)
    {
        if(db == null)
        {
            db = new DatabaseConnector(dbHostName, dbUserName, dbPassWord);
        }
        return db;
    }

    public static void main(String args[])
    {
        /* *************
         *  Some tests *
         * *************/

        getInstance().pushToDB("testBool", "0");
        getInstance().pushToDB("testFloat", "0.684f");
    }
}