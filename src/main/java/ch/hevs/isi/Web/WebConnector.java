package ch.hevs.isi.Web;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.Core.IConnector;
import ch.hevs.isi.Field.BooleanRegister;
import ch.hevs.isi.Field.FloatRegister;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;

/** This is a singleton class
 *  It displays FloatDataPoints or BooleanDataPoints and push it to a website
 *  Messages can be send and some commands can be send from the website
 */

public class WebConnector extends WebSocketServer implements IConnector {

    /* **********************
     *  Declare attributes  *
     * **********************/

    private static WebConnector wc = null;                          // Singleton class's attribute
    public ArrayList<WebSocket> wsArrayList = new ArrayList<>();    // Using to store all webSockets

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Inform connector that the specified object has a new value
     *  @param bDataPoint the Boolean DataPoint
     */

    public void onNewValue(BooleanDataPoint bDataPoint)
    {
        BooleanRegister aBooleanRegister = BooleanRegister.getRegisterFromDatapoint(bDataPoint);
        aBooleanRegister.write();                                       // Write in modBus the current value
        String valueToPush;                                             // The value to push on web pages

        // Convert the value to a String
        if(bDataPoint.getValue() == true)
        {
            valueToPush = "1";
        }
        else
        {
            valueToPush = "0";
        }
        pushToWebPages(bDataPoint.getLabel(),valueToPush);              // Push new value to web pages
    }

    /** Inform connector that the specified object has a new value
     *  @param fDataPoint the float DataPoint
     */

    public void onNewValue(FloatDataPoint fDataPoint)
    {
        FloatRegister aFloatRegister = FloatRegister.getRegisterFromDatapoint(fDataPoint);
        aFloatRegister.write();                                         // Write in modBus the current value

        String valueToPush = String.valueOf(fDataPoint.getValue());     // Convert float value to a String
        pushToWebPages(fDataPoint.getLabel(),valueToPush);              // Push the new value to web pages
    }

    private WebConnector()
    {
        super(new InetSocketAddress("localhost", 8888));    // Init webSocketServer constructor parameters
        start();                                                            // Starts the server that binds to the currently set port number and listeners for WebSocket connection requests
    }

    /** Called after an opening handshake has been performed and the given websocket is ready to be written on.
     *
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param clientHandshake The handshake of the websocket instance
     */

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake)
    {
        webSocket.send("Welcome to the server !");
        wsArrayList.add(webSocket);                                         // Add the webSocket to the arrayList
    }

    /** Called after the websocket connection has been closed.
     *
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param i The codes can be looked up
     * @param s Additional information string
     * @param b Returns whether or not the closing of the connection was initiated by the remote host.
     */

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        webSocket.send("Goodbye Server !");
        webSocket.close();                                                  // Close the WebSocket connection or connection attempt
        wsArrayList.remove(webSocket);                                      // Remove the webSocket from the arrayList
    }

    /** Callback for string messages received from the remote host
     *
     * @param webSocket The WebSocket instance this event is occuring on.
     * @param s The UTF-8 decoded message that was received.
     */

    @Override
    public void onMessage(WebSocket webSocket, String s)
    {
        String[] aStringArr = s.split("=");
        if(aStringArr[1].equalsIgnoreCase("true") || aStringArr[1].equalsIgnoreCase("false"))
        {
            BooleanDataPoint bDataPoint = (BooleanDataPoint) DataPoint.getDataPointFromLabel(aStringArr[0]);
            bDataPoint.setValue(Boolean.valueOf(aStringArr[1]));
        }
        else
        {
            FloatDataPoint fDataPoint = (FloatDataPoint) DataPoint.getDataPointFromLabel(aStringArr[0]);
            fDataPoint.setValue(Float.valueOf(aStringArr[1]));
        }
    }

    /** Called when errors occurs. If an error causes the websocket connection to fail onClose(WebSocket, int, String, boolean) will be called additionally.
     This method will be called primarily because of IO or protocol errors.
     If the given exception is an RuntimeException that probably means that you encountered a bug.
     *
     * @param webSocket Can be null if there error does not belong to one specific websocket. For example if the servers port could not be bound.
     * @param e The exception causing this error
     */

    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }

    /** Called when the server started up successfully. If any error occured, onError is called instead.
     *
     */

    @Override
    public void onStart() {

    }

    private void pushToWebPages(String  label, String value)
    {
        //Display label and value
        String sOut = "WebConnector <" + label + ", " + value + ">";
        System.out.println(sOut);

        // Send to all webSockets
        for(int i = 0; i < wsArrayList.size(); i++)
        {
            wsArrayList.get(i).send(label + "=" + value);
        }
    }

    /** Get the reference to private attribute of the singleton class
     *  @return the singleton class's private attribute
     */

     public static WebConnector getInstance()
    {
        if(wc == null)
        {
            wc = new WebConnector();
        }
        return wc;
    }
}
