package ch.hevs.isi.Field;

import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/** The class who implement modBus methods and make the connections
 *
 */

public class ModbusAccessor {

    /* *********************
     *  Declare attributes *
     * *********************/

    private Socket sock;
    private OutputStream out;
    private InputStream in;
    private static String _nameServer;
    private static int _portNbr;
    private static ModbusAccessor ma = null;                    // Singleton class's attribute
    private static final int MBAP_SIZE = 7;
    private static final int PDU_REQUEST_SIZE = 5;
    private static final int PDU_WRITE_REQUEST_SIZE = 10;
    private static final int PDU_RESPONSE_SIZE = 4;

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Get the reference to private attribute of the singleton class
     * @return the singleton class's private attribute
     */

    public static ModbusAccessor getInstance()
    {
        return getInstance("localhost", 1502);
    }

    /** Get the reference to private attribute of the singleton class
     *
     * @param address the address to connect with
     * @param port the port to connect with
     * @return the singleton class's private attribute
     */

    public static ModbusAccessor getInstance(String address, int port)
    {
        if(ma == null)
        {
            ma = new ModbusAccessor(address, port);
        }
        return ma;
    }

    private ModbusAccessor(String nameServer, int portNbr)
    {
        _nameServer = nameServer;
        _portNbr = portNbr;

        try {
            sock = new Socket(_nameServer, _portNbr);       // Create new Socket with specified server name & port number
            out = sock.getOutputStream();
            in = sock.getInputStream();
            System.out.println("Modbus TCP Client connected to : " + _nameServer + " server and " + _portNbr + " port.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Disconnect the Modbus TCP Client
     */

    public void disconnect()
    {
        if (sock != null) {
            try {
                sock.close();
                out.close();
                in.close();
                System.out.println("Modbus TCP Client Disonnected.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** Manage Mbap header from data from tcp client
     *  @return the array conversion of the ByteBuffer
     */

    private void manageMbapHeader(ByteBuffer bb, byte unitId, int length)
    {
        // Fill MBAP Header
        bb.putShort((short) 0xf0f0);                          // Transaction identifier (Request/Response)
        bb.putShort((short) 0);                               // Protocol Identifier (0 = Modbus Protocol)
        bb.putShort((short) ((length + 1) & 0xffff));         // Number of following bytes
        bb.put((byte) unitId);                                // Id of a remote slave connected on a serial line or on other buses
    }

    /** Manage Pdu from data from tcp client
     *
     * @param functionCode the number corresponding of the function code
     * @param startingAddressToRead the starting address to read
     * @param value the quantity of registers to read or the new Value to write in registers
     * @return the array conversion of pduBuffer
     */

    private void manageReadPDU(ByteBuffer bb, int functionCode, int startingAddressToRead, float value)
    {
        // Fill Request
        bb.put((byte) (functionCode & 0xff));                        // Function Code
        bb.putShort((short) (startingAddressToRead & 0xffff));       // Starting address to read
        bb.putShort((short) ((int) value & 0xffff));                 // Nbr Register to read or new value to assign
    }

    private void manageWritePDU(ByteBuffer bb, int functionCode, int startingAddressToRead, int qtyOfRegToRead, float value) {

        // Fill Request
        bb.put((byte) (functionCode & 0xff));                        // Function Code
        bb.putShort((short) (startingAddressToRead & 0xffff));       // Starting address to read
        bb.putShort((short) (qtyOfRegToRead & 0xffff));              // Quantity of registers to read
        bb.put((byte) (((2*qtyOfRegToRead) & 0xff)));                // ByteCount
        bb.putFloat(value);                                          // new value to assign

    }

    private byte[] readRegisters(int unitId, byte function, int startingAddressToRead, int qtyOfRegToRead) {
        // Check if the socket is connected
        if (sock.isConnected()) {
            // Check if the function entered is valid here
            if(function == 3 || function == 4) {
                // Check if the quantity of the registers to read is valid
                if(qtyOfRegToRead >= 0x0001 && qtyOfRegToRead <= 0x007D)
                {
                    ByteBuffer bufferToSend = ByteBuffer.allocate(MBAP_SIZE + PDU_REQUEST_SIZE);
                    bufferToSend.order(ByteOrder.BIG_ENDIAN);
                    manageMbapHeader(bufferToSend, (byte) (unitId & 0xff), 5);              // Fill MBAP Header
                    manageReadPDU(bufferToSend,function, startingAddressToRead, qtyOfRegToRead);   // Fill PDU

                    // Write the byteBuffer in the OutputStream
                    try {
                        //System.out.println(Utility.getHexString(bufferToSend.array()));
                        out.write(bufferToSend.array());
                        out.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    System.out.println("Quantity of register to read not valid. Error 02");
                }
            }
            else{
                System.out.println("Function not valid. Error 01");
            }
        }
        else
        {
            System.out.println("Socket not connected.");
        }
        return null;
    }

    private byte[] readInputRegister(int unitId, int startingAddressToRead, int qtyOfRegToRead)
    {
        return readRegisters(unitId, (byte) 0x04, startingAddressToRead, qtyOfRegToRead);
    }

    private byte[] readHoldingRegister(int unitId, int startingAddressToRead, int qtyOfRegToRead)
    {
        return readRegisters(unitId, (byte) 0x03, startingAddressToRead, qtyOfRegToRead);
    }

    /** Read a float from register
     * @param regAddress the address to starting to read
     * @return the value of the register
     */

    public Float readFloat(int unitId, byte function, int regAddress, int qtyOfRegToRead) {
        readRegisters(unitId, function, regAddress, qtyOfRegToRead);
        ByteBuffer bufferToRead = ByteBuffer.allocate(MBAP_SIZE + PDU_RESPONSE_SIZE);
        byte[] byteArray = new byte[1024];
        try {
            if (in.read(byteArray) > -1) {
                bufferToRead = ByteBuffer.wrap(byteArray);

                // Decode array
                int transaction = bufferToRead.getShort(0);
                int protocolId = bufferToRead.getShort();
                int length = bufferToRead.getShort();
                int unitId_ = bufferToRead.get();
                int functionCode = bufferToRead.get();
                int byteCount = bufferToRead.get();
                float registerValue = bufferToRead.getShort();
                Float value = bufferToRead.getFloat(9);
                return value;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /** Read a boolean in the register
     * @param refAddress the address to read
     * @return return the boolean value
     */

    public boolean readBoolean(int unitId, byte function, int refAddress,int qtyOfRegToRead) {
        float value = readFloat(unitId, function, refAddress, qtyOfRegToRead);
        if (value == 0f) {
            return false;
        } else {
            return true;
        }
    }

    private void writeMultipleRegister(int unitId, int startingAddressToRead, int qtyOfRegisterToRead, float newValue) {
        if (sock.isConnected()) {
            if(qtyOfRegisterToRead >= 0x0001 && qtyOfRegisterToRead <= 0x007B) {
                    ByteBuffer bufferToSend = ByteBuffer.allocate(MBAP_SIZE + PDU_WRITE_REQUEST_SIZE);
                    bufferToSend.order(ByteOrder.BIG_ENDIAN);
                    manageMbapHeader(bufferToSend, (byte) (unitId & 0xff), 5);
                    manageWritePDU(bufferToSend, 0x10, startingAddressToRead, qtyOfRegisterToRead, newValue);

                    // Write the byteBuffer in the OutputStream
                    try {
                        //System.out.println(Utility.getHexString(bufferToSend.array()));
                        out.write(bufferToSend.array());
                        out.flush();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                    {
                        System.out.println("Error 03");
                    }
        }
        else
        {
            System.out.println("Socket not connected.");
        }
    }

    /** Write a float in the holding register
     * @param refAddress the address to write the value
     * @param newValue   the new value to write in the register
     */

    public Float writeFloat(int unitId,int refAddress, int qtyOfRegToRead, float newValue)
    {
        writeMultipleRegister(unitId, refAddress, qtyOfRegToRead, newValue);
        ByteBuffer bufferToRead;
        byte[] byteArray = new byte[1024];
        try {
            if (in.read(byteArray) > -1) {
                bufferToRead = ByteBuffer.wrap(byteArray);

                // Decode the Mbap and PDU in the buffer
                int transaction = bufferToRead.getShort(0);
                int protocolId = bufferToRead.getShort();
                int length = bufferToRead.getShort();
                int unitId_ = bufferToRead.get();
                int functionCode = bufferToRead.get();
                int registerAddress = bufferToRead.getShort();
                int qtyOfRegistersToRead = bufferToRead.getShort();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /** Write a boolean in the holding register
     * @param regAddress the address of the register to write
     * @param newValue   the boolean value to write
     */

    public void writeBoolean(int unitId, int regAddress, int qtyOfRegToRead, boolean newValue) {
        if (newValue) {
            writeFloat(unitId, regAddress, qtyOfRegToRead,1f);
        } else {
            writeFloat(unitId, regAddress, qtyOfRegToRead, 0f);
        }
    }

    public static void main (String[]args)
    {
        /* *************
         *  Some tests *
         * *************/

        // ModbusAccessor mba = new ModbusAccessor("localhost", 1502);       // Singleton class's attribute
        //System.out.println("Float value : " + mba.readFloat(1,(byte) 0x03,613, 2));
        //System.out.println("Boolean value : " + mba.readBoolean(1,(byte) 0x03,613, 1));
        //getInstance().writeFloat(1,209, 2,0.75f);
        //mba.writeBoolean(1,405, 2,false);
        //mba.writeFloat(1,205, 2,0.0f);
        //System.out.println("Float value : " + mba.readFloat(1, (byte) 0x03,209,2));
        //System.out.println("Float value : " + getInstance().readFloat(1,(byte) 3,89,2));
        //System.out.println("Boolean value : " + mba.readBoolean(1,(byte) 0x03,613, 1));
        //mba.writeFloat(1,405,2,1);
    }
}
