package ch.hevs.isi.Field;

import ch.hevs.isi.Core.BooleanDataPoint;
import java.util.HashMap;

/**
 * BooleanRegister is a register who contains one BooleanDataPoint in a certain address
 */

public class BooleanRegister  {

    /* **********************
     *  Declare attributes  *
     * **********************/

    ModbusAccessor ma = ModbusAccessor.getInstance();
    BooleanDataPoint _data;
    int _address;

    //The Map who "connect" all DataPoint to the corresponding Register
    private static HashMap<BooleanDataPoint , BooleanRegister> BoolDataPointsMap = new HashMap<>();

    /* ******************************
     *  Declare & implement methods *
     * ******************************/

    /** Construct the Boolean Register
     * @param address is the address of the register
     */

    public BooleanRegister(String label, boolean isInput, int address)
    {
        _data = new BooleanDataPoint(label, isInput);
        _address = address;
        BoolDataPointsMap.put(_data, this);
    }

    /** Read a boolean Data point from modBus and Update the value of _data
     * @return the BooleanDatapoint updated
     */

    public BooleanDataPoint read()
    {
        boolean update = ma.readBoolean(1,(byte) 3, _address, 2);
        _data.setValue((update));
        return _data;
    }

    /**
     * Write the current value of _data in Modbus
     */

    public void write()
    {
        boolean update =  _data.getValue();
        ma.writeBoolean(1, _address, 2, update);
    }

    /** Get register from a BooleanDataPoint
     * @param bp The BooleanDataPoint
     * @return the corresponding BooleanRegister
     */

    public static BooleanRegister getRegisterFromDatapoint(BooleanDataPoint bp)
    {
        return BoolDataPointsMap.get(bp);
    }

    /**
     * Function who update all input register
     */

    public static void  poll()
    {
        for (BooleanDataPoint bdp : BoolDataPointsMap.keySet())         // loop over all BooleanDataPoint
        {
            if(bdp.isInput())
            {
                (BoolDataPointsMap.get(bdp)).read();                   // read method of the corresponding register
            }
        }
    }
}
