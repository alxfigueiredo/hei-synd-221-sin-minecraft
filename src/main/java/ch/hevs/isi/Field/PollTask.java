package ch.hevs.isi.Field;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The class used to poll the registers
 */

public class PollTask extends TimerTask {

    @Override public void run() {
        BooleanRegister.poll();
        FloatRegister.poll();
    }
}
