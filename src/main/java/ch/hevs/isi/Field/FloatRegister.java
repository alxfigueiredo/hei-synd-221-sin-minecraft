package ch.hevs.isi.Field;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.Field.ModbusAccessor;
import java.util.HashMap;

/**
 * FloatRegister is a register who contains one FloatDataPoint in a certain address
 */

public class FloatRegister {

    /* **********************
     *  Declare attributes  *
     * **********************/

    public FloatDataPoint _data;
    int _range;
    int _address;
    int _offset;

    // The Map who "connect" all DataPoint to the corresponding Register
    private static HashMap<FloatDataPoint , FloatRegister> FloatDataPointsMap = new HashMap<>();

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Constuct a float register
     * @param range is the range of the values in the register, the value is between 0 and 1, so range is the max value of the data
     * @param address is the address of the register
     */

    public FloatRegister(String label, Boolean isInput, int address, int range, int offset)
    {
        _data = new FloatDataPoint(label, isInput);
        _range = range;
        _address = address;
        _offset = offset;
        FloatDataPointsMap.put(_data,this);
    }

    /** Read a float Data point from modBus and Update the value of _data
     * @return the FloatDatapoint updated
     */

    public  FloatDataPoint read()
    {
        float update = ModbusAccessor.getInstance().readFloat(1,(byte) 3, _address, 2);
        _data.setValue((_range * update) + _offset);
        return _data;
    }

    /**
     * Write the current value of _data in Modbus
     */

    public void write()
    {
        float update =  _data.getValue();
        ModbusAccessor.getInstance().writeFloat(1, _address, 2,update);
    }

    /** Get register from a FloatDataPoint
     * @param fp The FloatDataPoint
     * @return the corresponding FloatRegister
     */

    public static FloatRegister getRegisterFromDatapoint(FloatDataPoint fp)
    {
        return FloatDataPointsMap.get(fp);
    }

    /**
     * Function who update all input register
     */

    public static void  poll()
    {
        for (FloatDataPoint fdp : FloatDataPointsMap.keySet()) {
            if(fdp.isInput())
            {
                (FloatDataPointsMap.get(fdp)).read();
            }
        }
    }
}
