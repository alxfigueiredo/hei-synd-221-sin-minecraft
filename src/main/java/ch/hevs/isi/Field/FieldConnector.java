package ch.hevs.isi.Field;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.Core.IConnector;
import ch.hevs.isi.Field.BooleanRegister;
import java.util.HashMap;
import java.util.Timer;

import static ch.hevs.isi.Field.BooleanRegister.*;

/** This is a singleton class
 *  It display the FloatDataPoint or the BooleanDataPoint to a field
 */

public class FieldConnector implements  IConnector {

    /* **********************
     *  Declare attributes  *
     * **********************/

    //private static FieldConnector fc = new FieldConnector();           // Singleton class's attribute
    private static FieldConnector fc = null;
    Timer pollTime;

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Inform connectors that the specified object has a new value
     *  @param bDataPoint the Boolean DataPoint
     */

    public void onNewValue(BooleanDataPoint bDataPoint)
    {
        BooleanRegister br = BooleanRegister.getRegisterFromDatapoint(bDataPoint);
        br.write();
        String valueToPush;

        // Convert the value to a String
        if(bDataPoint.getValue() == true)
        {
            valueToPush = "1";
        }
        else
        {
            valueToPush = "0";
        }
        pushToField(bDataPoint.getLabel(),valueToPush);                // Push new value to Field
    }

    /** Inform connector that the specified object has a new value
     *  @param fDataPoint the float DataPoint
     */

    public void onNewValue(FloatDataPoint fDataPoint)
    {
        FloatRegister fr = FloatRegister.getRegisterFromDatapoint(fDataPoint);
        fr.write();
        String valueToPush = String.valueOf(fDataPoint.getValue());     // Convert float value to a String
        pushToField(fDataPoint.getLabel(),valueToPush);                 // Push the new value to Field
    }

    private FieldConnector()
    {

    }

    public void startPolling()
    {
        pollTime = new Timer();
        pollTime.scheduleAtFixedRate(new PollTask(),0,500);
    }

    private void pushToField(String  label, String value)
    {
        // display label and value
        String sOut = "FieldConnector <" + label + ", " + value + ">";
        System.out.println(sOut);
    }

    /** Get the reference to private attribute of the singleton class
     *  @return the singleton class's private instance
     */

    public static FieldConnector getInstance()
    {
        if(fc == null)
        {
            fc = new FieldConnector();
        }
        return fc;
    }
}
