package ch.hevs.isi.SmartControl;

import ch.hevs.isi.Core.FloatDataPoint;
import ch.hevs.isi.SmartControl.SmartControl;

import java.io.IOException;
import java.util.TimerTask;

import static ch.hevs.isi.SmartControl.SmartControl.doControl;
import static jdk.nashorn.internal.objects.Global.println;

public class SmartPoll extends TimerTask {

    /**
     * run the smartcontroll
     */
    @Override public void run() {
        try {
            System.out.println( SmartControl.doControl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
