package ch.hevs.isi.SmartControl;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.FloatDataPoint;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Timer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.PrintWriter;

/**
 * This class is used to do smart energy regulations in the system
 */

public class SmartControl
{
    /* *********************
     *  Declare attributes *
     * *********************/

    private  static SmartControl sc = null;     // Singleton class's attribute
    Timer _TimerUpdate;                         // timer who update each time de smartController
    static boolean inCharge ;                   // define if the battery must be in charge or not

    /* ******************************
     *  Declare & implement methods *
     * ******************************/

    /**
     * Constructor of the singleton Class
     */

    public SmartControl()
    {
        inCharge = false;
    }

    /**
     * startPoll is to start a polling with the timer
     */

    public void startPoll()
    {
        _TimerUpdate = new Timer();
        _TimerUpdate.scheduleAtFixedRate(new SmartPoll(),0,10000);
    }

    /** Get the reference to private attribute of the singleton class
     *  @return the singleton class's private instance
     */

    public static SmartControl getInstance() {
        return sc;
    }

    /** This is the regulation algorithm
     * @return the value of the register SCORE
     * @throws IOException , because there is some writing in a file SCORE.txt
     */

    public static float doControl() throws IOException {

        BooleanDataPoint windT = (BooleanDataPoint) BooleanDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        BooleanDataPoint solarT = (BooleanDataPoint) BooleanDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        FloatDataPoint battCharge = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");

        windT.setValue(true);       // Activate wind producer
        solarT.setValue(true);      // Activate solar panels

        // get all powers in
        float solarP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT")).getValue();
        float windP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WIND_P_FLOAT")).getValue();
        float coalP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_P_FLOAT")).getValue();
        float coalAmount = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_AMOUNT")).getValue();
        float battP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_P_FLOAT")).getValue();

        FloatDataPoint coalPercent = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP"));
        FloatDataPoint factoryPercent = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP"));

        // Calculate total power in
        float totalPowerIn = windP + solarP +coalP + battP;

        // Get all power out
        float homeP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("HOME_P_FLOAT")).getValue();
        float publicP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT")).getValue();
        float bunkerP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BUNKER_P_FLOAT")).getValue();
        float factoryP = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("FACTORY_P_FLOAT")).getValue();

        // Calculate Total power out
        float totalPowerOut = homeP + publicP + bunkerP+factoryP;
        float powerInOutDiff = totalPowerIn - totalPowerOut;

        //if batt is not in charge
        if((battCharge.getValue()>0.2f) && !(inCharge)) {
            // If energy is needed
            if (powerInOutDiff < 0) {
                powerInOutDiff=-powerInOutDiff;
                //if battP is giving power
                if ((battP<500)&&(battP>10)){
                    float pRestBatt = 500-battP;
                    //if power needed is bigger than battery can give
                    if (powerInOutDiff > pRestBatt) {
                        float pNeedMore = powerInOutDiff - pRestBatt ;
                        // if coal factory not working at 100%
                        if (coalPercent.getValue() < 1) {
                            float pRestCoal = 600 - coalP;
                            // coal factory can t produce enough energy
                            if (pNeedMore > pRestCoal) {
                                coalPercent.setValue(1);
                                factoryPercent.setValue(factoryPercent.getValue() - (pNeedMore - pRestCoal) / 1000);
                            } else {                        // if coal factory can produce the energy needed
                                coalPercent.setValue(coalPercent.getValue() + pNeedMore / 600);
                            }
                        }else{
                            factoryPercent.setValue(0);
                        }
                    } else {                                // if Pneeded is less than battery can give
                        pRestBatt = pRestBatt - powerInOutDiff;
                        if(coalP<pRestBatt){
                            coalPercent.setValue(0);
                            factoryPercent.setValue(factoryPercent.getValue()+(pRestBatt-coalP)/1000);
                        }else{
                            coalPercent.setValue(coalPercent.getValue()-pRestBatt/600);
                        }
                    }
                }else if(battP<0){                          // if batt is recharging but doesn t need
                    factoryPercent.setValue(factoryPercent.getValue()-battP/1000+0.2f);
                }else{
                    factoryPercent.setValue(factoryPercent.getValue()-(powerInOutDiff-500)/1000);
                }

            } else {                                        // too much energy
                if (factoryPercent.getValue() != 1) {       // if Factory is not working at 100%
                    float pRestFactory = 1000 - factoryP;
                    if (pRestFactory <= powerInOutDiff) {   //the energy needed for max is less than energy we have too much
                        factoryPercent.setValue(1);
                        coalPercent.setValue((float) (coalPercent.getValue() - 0.1f - (powerInOutDiff - pRestFactory) / 600));

                    } else {                                // if powerInOutDiff is not enough for Factory work at 100 %
                        factoryPercent.setValue(factoryPercent.getValue() + powerInOutDiff / 1000);
                    }
                }
            }
        }else if((inCharge)&&(battCharge.getValue()>=0.4f)){ //if batt is charging

            if(battCharge.getValue()>0.7f) {                 //if batt is more than 70% then stop charging
                inCharge=false;
                coalPercent.setValue(0);
            }else if(battCharge.getValue()>0.5f) {           // for the inerty if batt is up to 50% then begin to reduce coal industry
                coalPercent.setValue(0.5f);
            }
        }else if(!(inCharge)&&(battCharge.getValue()<0.25f)) {  // if batt need to be charged
            inCharge = true;
            factoryPercent.setValue(0);
            coalPercent.setValue(1);

        }else{
            coalPercent.setValue(1f);
        }

        //write the score in the file SCORE.txt
        float sc = ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SCORE")).getValue();
        File file = new File("SCORE.txt");
        FileWriter fr = new FileWriter(file, true);
        BufferedWriter br = new BufferedWriter(fr);
        PrintWriter pr = new PrintWriter(br);
        if(sc>0) {
            pr.println(sc);
        }
        pr.close();
        br.close();
        fr.close();

        return ((FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SCORE")).getValue();
    }
}
