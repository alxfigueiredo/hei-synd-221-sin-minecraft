package ch.hevs.isi.Core;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.FloatDataPoint;
import java.util.HashMap;

/**
 *  This class create an object named DataPoint
 *  There's a container HashMap to save all DataPoints
 *  DataPoint have two specialized class : BooleanDataPoint & FloatDataPoint
 */

public class DataPoint
{
    /* *********************
     *  Declare attributes *
     * *********************/

    private static HashMap<String, DataPoint> dataPointsMap = new HashMap<>();
    private boolean _isInput;
    private String _label;

    /* ******************************
     *  Declare & implement methods *
     * ******************************/

    /** Creates a DataPoint with the specified label and a I/O State
     *  @param label Key of the DataPoint in the Map
     *  @param isInput Value is true if the DataPoint is an input
     */

    DataPoint(String label, boolean isInput)
    {
        _label = label;
        _isInput = isInput;
        dataPointsMap.put(_label, this);    // Put the DataPoint just created on the map
        String test;
    }

    /** Get the label's key of the DataPoint
     *  @return the DataPoint label's key
     */

    public String getLabel()
    {
        return _label;
    }

    /** Is the DataPoint an input or an output ? Get it
     *  @return DataPoint state
     */

    public boolean isInput()
    {
        return _isInput;
    }

    /** Get a DataPoint from the HashMap with a label
     *  @param label DataPoint label's key
     *  @return the DataPoint with the specified label
     */

    public static DataPoint getDataPointFromLabel(String label)
    {
        return dataPointsMap.get(label);
    }

    public static void main(String[] args)
    {
        /* ******************************
         *  Test for boolean DataPoints *
         * ******************************/

        BooleanDataPoint a = new BooleanDataPoint("Dp1", true);     // Create a Boolean input DataPoint
        BooleanDataPoint b = new BooleanDataPoint("Dp2", false);    // Create a Boolean output DataPoint
        BooleanDataPoint w = (BooleanDataPoint) DataPoint.getDataPointFromLabel("Dp1");     // Create and assign from map

        a.setValue(true);                                                         // Set a value to a
        b.setValue(false);                                                        // Set a value to b

        /* ****************************
         *  Test for float DataPoints *
         * ****************************/

        FloatDataPoint c = new FloatDataPoint("Dp3", true);         // Create a Float input DataPoint
        FloatDataPoint d = new FloatDataPoint("Dp4", false);        // Create a Float output DataPoint

        new FloatDataPoint("DpNotSaved", true);                     // DataPoint not stored in the map
        FloatDataPoint y = (FloatDataPoint) DataPoint.getDataPointFromLabel("DpNotSaved");      // Get DataPoint not saved on map
        y.setValue(100f);

        c.setValue(50f);                                                          // Set a value to c
        d.setValue(25f);                                                          // Set a value to d
    }
}
