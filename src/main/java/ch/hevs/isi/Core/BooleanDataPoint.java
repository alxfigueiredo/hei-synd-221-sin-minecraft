package ch.hevs.isi.Core;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.Web.WebConnector;

/**
 * This class is a DataPoint specialized class
 * Specially used for booleans DataPoints's type
 * BooleanDataPoint can get and set the values
 * And inform relatives class about some changes
 */

public class BooleanDataPoint extends DataPoint
{
    /* **********************
     *  Declare attributes  *
     * **********************/

    private boolean _value;

    /* *******************************
     *  Declare & implement methods  *
     * *******************************/

    /** Create a BooleanDataPoint with the specified key label and I/O state
     * @param label Key of the DataPoint in the Map
     * @param isInput Value is true if the DataPoint is an input
     */

    public BooleanDataPoint(String label, boolean isInput)
    {
        super(label, isInput);
    }

    /** Get BooleanDataPoint value
     *  @return the DataPoint boolean Value
     */

    public boolean getValue()
    {
        return _value;
    }

    /** Set a new value on the DataPoint & inform changes to relatives class
     *  @param newValue new value to set
     */

    public void setValue(boolean newValue)
    {
        if(this != null)
        {
            _value = newValue;

            DatabaseConnector.getInstance().onNewValue(this);       // Inform DatabaseConnector that we have a new value
            WebConnector.getInstance().onNewValue(this);            // Inform WebConnector that we have a new value

            // If the DataPoint is an output, inform the field Connector we have a new Value
            if (isInput() == false) {
                FieldConnector.getInstance().onNewValue(this);
            }
        }
    }
}
