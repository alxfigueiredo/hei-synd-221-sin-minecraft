package ch.hevs.isi.Core;

import ch.hevs.isi.Core.BooleanDataPoint;
import ch.hevs.isi.Core.DataPoint;
import ch.hevs.isi.Core.FloatDataPoint;

/**
* Interface for all Connectors
*/

public interface IConnector {

    /** Inform connectors that the specified object has a new value
     *  @param bDataPoint the Boolean DataPoint
     */

    void onNewValue(BooleanDataPoint bDataPoint);

    /** Inform connectors that the specified object has a new value
     *  @param fDataPoint the float DataPoint
     */

    void onNewValue(FloatDataPoint fDataPoint);
}
