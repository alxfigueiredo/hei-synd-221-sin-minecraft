package ch.hevs.isi;

import ch.hevs.isi.Database.DatabaseConnector;
import ch.hevs.isi.Field.BooleanRegister;
import ch.hevs.isi.Field.FieldConnector;
import ch.hevs.isi.Field.FloatRegister;
import ch.hevs.isi.Field.ModbusAccessor;
import ch.hevs.isi.SmartControl.SmartControl;
import ch.hevs.isi.Web.WebConnector;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }

        ModbusAccessor.getInstance(modbusTcpHost, modbusTcpPort);
        DatabaseConnector.getInstance(dbHostName, dbUserName, dbPassword);
        WebConnector.getInstance();

        // Stream read
        try
        {
            // set relative path
            Path current = Paths.get("csvFiles");
            String s = current.toAbsolutePath().toString();

            // File stream
            FileReader f = new FileReader(s+"\\FloatRegisters.csv");
            FileReader b = new FileReader(s+"\\BooleanRegisters.csv");

            // File manipulator
            BufferedReader inputFloat = new BufferedReader(f);
            BufferedReader inputBool = new BufferedReader(b);

            String floatLine;
            String boolLine;

            // Check if the float buffer contain something
            while((floatLine = inputFloat.readLine()) != null)
            {
                String[] floatRegisterParameters = floatLine.split(";");
                FloatRegister fr = new FloatRegister(floatRegisterParameters[0], Boolean.valueOf(floatRegisterParameters[1]), Integer.valueOf(floatRegisterParameters[2]),
                        Integer.valueOf(floatRegisterParameters[3]), Integer.valueOf(floatRegisterParameters[4]));
            }

            // Check if the boolean buffer contain something
            while((boolLine = inputBool.readLine()) != null)
            {
                String[] boolRegisterParameters = boolLine.split(";");
                new BooleanRegister(boolRegisterParameters[0], Boolean.valueOf(boolRegisterParameters[1]), Integer.valueOf(boolRegisterParameters[2]));
            }

            // close the files now
            inputFloat.close();
            inputBool.close();
        }
        catch(IOException e)
        {
            System.out.print("File not found or could not be opened");
            e.printStackTrace();
        }
        FieldConnector.getInstance().startPolling();
        SmartControl sc = new SmartControl();
        sc.startPoll();
    }
}
